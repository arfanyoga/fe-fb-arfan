// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, connectAuthEmulator } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// const firebaseConfig = {
//   apiKey: "AIzaSyC4TGx7swilPfr9dOX5BgBf4UCcoKznQZg",
//   authDomain: "fir-class-86db0.firebaseapp.com",
//   projectId: "fir-class-86db0",
//   storageBucket: "fir-class-86db0.appspot.com",
//   messagingSenderId: "514334206257",
//   appId: "1:514334206257:web:73cd10e3b7d8bbb4b9a810",
//   measurementId: "G-JMTMQ59NN8"
// };

const firebaseConfig = {
  apiKey: "AIzaSyAtUo55fpdto0e89gyze1GeUrSYabJgM2w",
  authDomain: "crewing-f9aad.firebaseapp.com",
  projectId: "fcrewing-f9aad",
  storageBucket: "crewing-f9aad.appspot.com",
  messagingSenderId: "948340317244",
  appId: "1:948340317244:web:b629a1724282e3d009b739",
  // measurementId: "G-JMTMQ59NN8"
};

// FIREBASE_API_KEY=AIzaSyAtUo55fpdto0e89gyze1GeUrSYabJgM2w
// FIREBASE_AUTH_DOMAIN=crewing-f9aad.firebaseapp.com
// FIREBASE_PROJECT_ID=fcrewing-f9aad
// FIREBASE_STORAGE_BUCKET=crewing-f9aad.appspot.com
// FIREBASE_MESSAGING_SENDER_ID=948340317244
// FIREBASE_APP_ID=1:948340317244:web:b629a1724282e3d009b739
// FIREBASE_MEASUREMENT_ID=
// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);
connectAuthEmulator(auth, "http://192.168.18.170:9099");
//connectFunctionsEmulator(app.functions(), '192.168.18.170', 5001);
export default app;