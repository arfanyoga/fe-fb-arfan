

import axios from 'axios'
import app from '../../config/firebase'
import {
    createUserWithEmailAndPassword, signInWithEmailAndPassword, sendEmailVerification,
    getAuth, signInWithPopup, GoogleAuthProvider
} from 'firebase/auth';
import { auth } from '../../config/firebase';
import { getFunctions, httpsCallable, connectFunctionsEmulator } from "firebase/functions";
import { useState } from 'react';

const Login = () => {

    const [email, setEmail] = useState();
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [role, setRole] = useState();
    const [emailReg, setEmailReg] = useState();
    const [password, setPassword] = useState();
    const [passwordReg, setPasswordReg] = useState();

    const registerHandler = async (e) => {
        e.preventDefault()

        await createUserWithEmailAndPassword(auth, emailReg, passwordReg)
            .then(async (userCredential) => {
                const user = userCredential.user;
                await sendEmailVerification(user);
                axios.post('https://us-central1-crewing-f9aad.cloudfunctions.net/api/users/',
                    {
                        firstName: firstName,
                        lastName: lastName,
                        roles: role,
                        customId: user.uid
                    }).then(res => { console.log('res', res) })

            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(errorCode, errorMessage);
                // ..
            });
    }


    const loginHandler = async (e) => {
        e.preventDefault()

        await signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                console.log(userCredential);
                //navigate("/login")
                const headers = {
                    'Authorization': 'Bearer ' + userCredential._tokenResponse.idToken,
                    'Content-Type': 'application/json'
                };


            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(errorCode, errorMessage);
                // ..
            });

    }

    const callableHandler = () => {

        const functions = getFunctions(app, 'us-central1');
        connectFunctionsEmulator(functions, "192.168.18.170", 5001);
        const addMessage = httpsCallable(functions, 'helloworld');
        addMessage({})
            .then((result) => {
                // Read result of the Cloud Function.
                /** @type {any} */
                const data = result.data;
                console.log(data);
                const sanitizedMessage = data.text;
            });
    }
    const googleHandler = () => {
        const auth = getAuth(App);
        signInWithPopup(auth, provider)
            .then((result) => {
                // This gives you a Google Access Token. You can use it to access the Google API.
                const credential = GoogleAuthProvider.credentialFromResult(result);
                const token = credential.accessToken;
                // The signed-in user info.
                const user = result.user;
                console.log(user)

                // IdP data available using getAdditionalUserInfo(result)
                // ...
            }).catch((error) => {
                // Handle Errors here.
                const errorCode = error.code;
                const errorMessage = error.message;
                // The email of the user's account used.
                console.log(error)
                const email = error.customData.email;
                // The AuthCredential type that was used.
                const credential = GoogleAuthProvider.credentialFromError(error);
                // ...
            });
    }
    const editHandler = async () => {

        const token = await auth.currentUser.getIdToken(true);
        console.log(token);
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token, // Add any additional headers as needed
        };

        // Make a PUT request using Axios
        axios.put('https://us-central1-crewing-f9aad.cloudfunctions.net/api/users/di5A74iUayuhbjiUKQGDFrMHwQ3y', {
            firstName: "kuedit12",
            lastName: "kuedit1",
            roles: "kuedit"
        }, { headers })
            .then(response => {
                console.log('Response:', response.data);
                // Handle successful response
            })
            .catch(error => {
                console.error('Error:', error);
                // Handle error
            });
    }

    return (
        <>
            <div>
                <h2>Register</h2>
                <form onSubmit={registerHandler}>
                    <input type="firstName" placeholder="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
                    <input type="lastName" placeholder="lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
                    <input type="role" placeholder="role" value={role} onChange={(e) => setRole(e.target.value)} required />

                    <input type="email" placeholder="Email" value={emailReg} onChange={(e) => setEmailReg(e.target.value)} required />
                    <input type="password" placeholder="Password" value={passwordReg} onChange={(e) => setPasswordReg(e.target.value)} required />
                    <button type="submit">Register</button>
                </form>
            </div>
            <button onClick={callableHandler}>get in with google</button>
            <button onClick={editHandler}>trigger edit</button>

            <div>
                <h2>Login</h2>
                <form onSubmit={loginHandler}>
                    <input type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    <input type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required />
                    <button type="submit">login</button>
                </form>
            </div>
        </>
    )
}


export default Login