
import axios from 'axios'
import app from '../../config/firebase'
import {
    createUserWithEmailAndPassword, signInWithEmailAndPassword, sendEmailVerification,
    getAuth, signInWithPopup, GoogleAuthProvider
} from 'firebase/auth';
import { auth } from '../../config/firebase';
import { getFunctions, httpsCallable, connectFunctionsEmulator } from "firebase/functions";
import { useState } from 'react';
const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const [error, setError] = useState(null);

    const registerHandler = async (e) => {
        e.preventDefault()

        await createUserWithEmailAndPassword(auth, email, password)
            .then(async (userCredential) => {
                const user = userCredential.user;
                await sendEmailVerification(user);
                const formData = new FormData();
                formData.append('firstName', firstName);
                formData.append('lastName', lastName);
                formData.append('id', user.uid);
                console.log(firstName, lastName)
                axios.post('http://192.168.18.170:5001/fir-class-86db0/us-central1/api/user/',
                    formData).then(res => { console.log('res', res) })

            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(errorCode, errorMessage);
                // ..
            });
    }

    return (
        <div>
            <h2>Register</h2>
            {error && <div>{error}</div>}
            <form onSubmit={registerHandler}>
                <div>
                    <label>Email:</label>
                    <input
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </div>
                <div>
                    <label>Password:</label>
                    <input
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </div>

                <div>
                    <label>lastName:</label>
                    <input
                        type="text"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        required
                    />
                </div>

                <div>
                    <label>firstName:</label>
                    <input
                        type="text"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        required
                    />
                </div>
                <button type="submit">register</button>
            </form>
        </div>
    );
};

export default Register;
