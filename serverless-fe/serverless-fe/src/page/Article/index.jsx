import { useEffect, useState } from "react"
import axios from "axios";
import { auth } from '../../config/firebase';
import { collection, doc, getFirestore, onSnapshot, snapshotEqual } from "firebase/firestore";
import firebase from "firebase/compat/app";
import app from '../../config/firebase'
const Article = () => {
    const [title, setTitle]=  useState();
    const [editId, setEditId] = useState();
    const [content, setContent]=  useState();
    const [thumbnail, setThumbnail]=  useState(null);
    const [data, setData] = useState([]);
    const [dataLogs, setDataLogs] = useState([]);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const staffId = "5qrVqwJJ91di7c7p8keA";
    const accountId = "EvGis9h2mOr6vA3VVULw";
    const db = getFirestore(app)
    
    const historiesRef = collection(db, 'accounts', accountId, 'staffs', staffId, 'histories');
    
    onSnapshot(historiesRef, snapshot => {
        console.log("here is some update");
    });
    useEffect(() => {
        
        const unsubscribe = auth.onAuthStateChanged(user => {
            setIsAuthenticated(!!user); // Set isAuthenticated to true if user is not null
        });
        return () => unsubscribe();
    }, []);

    useEffect(() => {
        if (isAuthenticated) {
            getData();
        }
    }, [isAuthenticated]);

    const editData = async (id) => {
        setEditId(id)
        let token = await auth.currentUser.getIdToken(true);
        console.log(token);
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token, // Add any additional headers as needed
        };

        // Make a PUT request using Axios
        axios.get('https://us-central1-crewing-f9aad.cloudfunctions.net/api/companies/', { headers })
            .then(response => {
                console.log('Response:', response.data);
                // Handle successful response
            })
            .catch(error => {
                console.error('Error:', error);
                // Handle error
            });

        
    
    }

    const editHandler = async (e) => {
        e.preventDefault();
        let token = await auth.currentUser.getIdToken(true);
        console.log(token);
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token, // Add any additional headers as needed
        };

        // Make a PUT request using Axios
        axios.put('https://us-central1-crewing-f9aad.cloudfunctions.net/api/companies/'+editId, {
            title: title,
            content: content,
            thumbnail: thumbnail
        }, { headers })
            .then(response => {
                console.log('Response:', response.data);
                getData();
                // Handle successful response
            })
            .catch(error => {
                console.error('Error:', error);
                // Handle error
            });
    }


    const deleteData = async (id)  => {
        let token = await auth.currentUser.getIdToken(true);
        console.log(token);
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token, // Add any additional headers as needed
        };

        // Make a PUT request using Axios
        axios.delete('https://us-central1-crewing-f9aad.cloudfunctions.net/api/companies/'+id, { headers })
            .then(response => {
                getData();
                console.log('Response:', response.data);
                // Handle successful response
            })
            .catch(error => {
                console.error('Error:', error);
                // Handle error
            });
    }

    const handleFileChange = (e) => {
        setThumbnail(e.target.files[0]);
    };

    const addHandler = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("title", title);
        formData.append("content", content);
        formData.append("file", thumbnail);

        try {
            const token = await auth.currentUser.getIdToken(true);
            const headers = {
                'enctype': 'multipart/form-data',
                'Authorization': 'Bearer ' + token,
            };

            const response = await axios.post('https://us-central1-crewing-f9aad.cloudfunctions.net/api/companies', formData, { headers });
            console.log('Response:', response.data);
            
            getData();
            setTitle("");
            setContent("");
            setThumbnail(null);
            
        } catch (error) {
            console.error('Error:', error);
            // Handle error
        }
    };

    const getData = async () => {
        const response = await axios.get('https://us-central1-crewing-f9aad.cloudfunctions.net/api/user/me', { headers });

        // Ensure that there is a signed-in user
        if (auth.currentUser) {
            try {
                let token = await auth.currentUser.getIdToken(true);
    
                const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token,
                };
    
                // Make a GET request using Axios
    
                console.log('Response:', response.data);
                setData(response?.data?.data);
                // Handle successful response
                const logs = await axios.get('https://us-central1-crewing-f9aad.cloudfunctions.net/api/logs', { headers });
                setDataLogs(logs?.data?.data);
                console.log(dataLogs)

            } catch (error) {
                console.error('Error:', error);
                // Handle error
            }
        } else {
            console.log('No user signed in');
            // Handle the case when no user is signed in
        }
    };
    getData()

    return (
        <>
        <form onSubmit={addHandler}>
    <input type="text" placeholder="title" value={title} onChange={(e) => setTitle(e.target.value)} required />
    <input type="text" placeholder="content" value={content} onChange={(e) => setContent(e.target.value)} required />
    <input type="file" onChange={(e) => handleFileChange(e)} required />
    <button type="submit">Submit</button>
</form>
        <table>
        <thead>
        <tr>
            <th>thumbnail</th>
            <th>id</th>
            <th>title</th>
            <th>created by</th>
            <th>updated by</th>
            <th>action</th>

        </tr>
        </thead>
        <tbody>
        {
    data?.map(item => (
        <tr key={item.id}> {/* Make sure to specify a unique key for each item */}
            <td> <img src={item.data.path} width="50" height="50" alt="" /> </td>
            <td>{item.id}</td>
            <td>{item.data.title}</td>
            <td>{item.data.createdBy}</td>
            <td>{item.data.editedBy}</td>
            <td>
                <button onClick={() => { editData(item.id) }}>edit</button>
                <button onClick={() => { deleteData(item.id) }}>delete</button>

            </td> {/* You may need to replace 'action' with actual action buttons */}
        </tr>
    ))
}
</tbody>

        </table>

        <form onSubmit={editHandler}>
                    <button type="submit">update</button>
        </form>

        <h5>logs</h5>
        <hr />
        <table>
        {dataLogs?.map(item => (
                        <tr key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.data.collection}</td>
                            <td>{item.data.activity}</td>
                            <td>{item.data.actor}</td>
                            <td>
                            {new Date(item.data.date).toString()}
                            </td>
                        </tr>
                    ))}
            
        </table>
        </>
    )    
}

export default Article