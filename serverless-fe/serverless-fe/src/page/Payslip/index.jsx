import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

const Payslip = () => {
    const { historyId, userId } = useParams();
    const [data, setData] = useState([]);

    useEffect(() => {
        // Define formData appropriately if needed
        const formData = {}; // Replace with actual formData if required

        axios.get(`https://us-central1-crewing-f9aad.cloudfunctions.net/api/payslip-history/payslip/${historyId}/${userId}`)
            .then(res => {
                console.log('res', res.data.data);
                setData(res.data.data); // Assuming the response data is an array
            })
            .catch(err => {
                console.error('Error fetching data', err);
            });
    }, []); // Empty dependency array to run only once on mount

    return (
        <>
            <div>User ID: {userId}</div>
            <div>History ID: {historyId}</div>
            {data ? (
                <div>
                <h1>
                    Pendapatan
                </h1>
                <table>
                    <thead>
                        <tr>
                            <th>Pendapatan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.sallarySummary?.income.map((item, index) => (
                            <tr key={index}>
                                <td>{item.name}</td> {/* Replace with actual data properties */}
                                <td>{item.amount}</td> {/* Replace with actual data properties */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <br />
                <br />
                <table>
                    <thead>
                        <tr>
                            <th>Potongan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.sallarySummary?.cuts.map((item, index) => (
                            <tr key={index}>
                                <td>{item.name}</td> {/* Replace with actual data properties */}
                                <td>{item.amount}</td> {/* Replace with actual data properties */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <br />
                <br />
                <table>
                    <thead>
                        <tr>
                            <th>Benefit</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.sallarySummary?.benefit.map((item, index) => (
                            <tr key={index}>
                                <td>{item.name}</td> {/* Replace with actual data properties */}
                                <td>{item.amount}</td> {/* Replace with actual data properties */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <br />
                <br />
                <table>
                    <thead>
                        <tr>
                            <th>Premi</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.sallarySummary?.premi.map((item, index) => (
                            <tr key={index}>
                                <td>{item.name}</td> {/* Replace with actual data properties */}
                                <td>{item.amount}</td> {/* Replace with actual data properties */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <br />
                
                <h1>Total</h1>
                <h3>Total Sallary : {data?.total?.totalSallary}</h3>
                <h3>Total Potongan :{data?.total?.totalCuts}</h3>
                <h3>Total Benefit :{data?.total?.totalBenefit}</h3>
                <h2>TAKE HOME PAY : {data?.takeHomePay}</h2>

                
                </div>
                
            ) : (
                <div>No data available</div>
            )}
        </>
    );
};

export default Payslip;
