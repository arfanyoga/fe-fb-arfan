// App.js
import React, { useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route, Link, useLocation, useNavigate } from 'react-router-dom';
import './App.css'

import Login from './page/Login';
import Article  from './page/Article'
import Register from './page/Register';
import Payslip from './page/Payslip';

const App = () => {
  return (
    
      <div>
<h1>tes coba</h1>
        <div className='pt-[70px]'>
        <Routes className>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/article" element={<Article />} />
          <Route path="/payslip/:historyId/:userId" element={<Payslip />} />


        </Routes>
        </div>
      </div>
   
  );
};

export default App;
